$(document).ready(function() {
    const base_url = "https://api.weather.gov/stations/";
    const endpoint = "/observations/latest";

    // weather update button click
    $('#getwx').on('click', function(e) {
        var mystation = $('input').val();
        var myurl = base_url + mystation + endpoint;
        $('input#my-url').val(myurl);

        // clear out any previous data
        $('ul li').each(function() {
            // enter code to clear each li
            $('.list-group').empty();
        });

        console.log("Cleared Elements of UL");

        // execute AJAX call to get and render data
        $.ajax({
            url: myurl,
            dataType: "json",
            success: function(data) {
                var tempC = data['properties']['temperature'].value.toFixed(1);
                var tempF = (tempC * 9 / 5 + 32).toFixed(1);
                var relativeHumidity = data['properties']['relativeHumidity'].value.toFixed(1);
                var windDirection = data['properties']['windDirection'].value.toFixed(0);
                var windSpeed = data['properties']['windSpeed'].value.toFixed(1);
                var iconURL = data['properties']['icon'];
                var icon = "<img src=\"" + iconURL + "\" alt=icon the shows ";
                var textDescription = data['properties']['textDescription'];

                // get wind info and convert m/s to kts
                // var windDirection = 
                // var windSpeed = 

                // uncomment this if you want to dump full JSON to textarea
                var myJSON = JSON.stringify(data);
                //$('textarea').val(myJSON);
                $('textarea').val(data.properties.rawMessage);

                var temperature = "<li>Current temperature: " + tempC + "C " + tempF + "F" + "</li>";
                var humidity = "<li>Humidity: " + relativeHumidity + " %RH" + "</li>";
                var currentWind = "<li>Current Wind: " + windDirection + " degrees at " + windSpeed + "kts" + "</li>";
                var pictureAndDescription = "<li>" + icon + "<br><br>" + textDescription + "</li>";

                // Adding temperature to table
                $('ul').append(temperature);
                $('ul li:last').attr('class', 'list-group-item');

                // Adding humidity to table
                $('ul').append(humidity);
                $('ul li:last').attr('class', 'list-group-item');

                // Adding wind direction and wind speed to table
                $('ul').append(currentWind);
                $('ul li:last').attr('class', 'list-group-item');

                // Adding icon and text description
                $('ul').append(pictureAndDescription);
                $('ul li:last').attr('class', 'list-group-item');

                // add additional code here for the Wind direction, speed, weather contitions and icon image
            }
        });
    });
});