// var stockData = [
//     { name: "Microsoft", marketCap: "$381.7 B", sales: "$86.8 B", profit: "$22.1 B", employees: "128,000" },
//     { name: "Nike", marketCap: "$83.1 B", sales: "$27.8 B", profit: "$2.7 B", employees: "56,500" },
//     { name: "Zulily", marketCap: "$2.9 B", sales: "$1.2 B", profit: "$14.9 B", employees: "2,907" },
//     { name: "Amazon", marketCap: "$144.3 B", sales: "$89.0 B", profit: "$-241 B", employees: "154,100" },
//     { name: "Google", marketCap: "$350.2 B", sales: "$55.2 B", profit: "$44.3 B", employees: "75,000" },
//     { name: "Costco Wholesale", marketCap: "$60.4 B", sales: "$60.4 B", profit: "$60.4 B", employees: "195,000" },
//     { name: "Alaska Air Group", marketCap: "$7.9 B", sales: "$5.4 B", profit: "$605 M", employees: "13,952" },
//     { name: "Nordstrom", marketCap: "$15.1 B", sales: "$12.5 B", profit: "$734 M", employees: "67,000" },
//     { name: "TrueBlue", marketCap: "$946 M", sales: "$2.2 B", profit: "$65.7 M", employees: "5,000" },
//     { name: "Nautilus", marketCap: "$476 M", sales: "$274.4 M", profit: "$18.8 M", employees: "340" }
// ];

var arrStocks = [];

function stock(companyName, marketCap, sales, profit, numberEmployees) {
    var tempObj = {};
    tempObj.CN = companyName;
    tempObj.MC = marketCap;
    tempObj.S = sales;
    tempObj.P = profit;
    tempObj.NE = numberEmployees;
    return tempObj;
}

arrStocks.push(stock("Microsoft", "$381.7 B", "$86.8 B", "$22.1 B", "128,000"));
arrStocks.push(stock("Nike", "$83.1 B", "$27.8 B", "$2.7 B", "56,500"));
arrStocks.push(stock("Zulily", "$2.9 B", "$1.2 B", "$14.9 B", "2,907"));
arrStocks.push(stock("Amazon", "$144.3 B", "$89.0 B", "$-241 B", "154,100"));
arrStocks.push(stock("Google", "$350.2 B", "$55.2 B", "$44.3 B", "75,000"));
arrStocks.push(stock("Costco Wholesale", "$60.4 B", "$60.4 B", "$60.4 B", "195,000"));
arrStocks.push(stock("Alaska Air Group", "$7.9 B", "$5.4 B", "$605 M", "13,952"));
arrStocks.push(stock("Nordstrom", "$15.1 B", "$12.5 B", "$734 M", "67,000"));
arrStocks.push(stock("TrueBlue", "$946 M", "$2.2 B", "$65.7 M", "5,000"));
arrStocks.push(stock("Nautilus", "$476 M", "$274.4 M", "$18.8 M", "340"));


const button = document.querySelector('.btn');
const table = document.querySelector('.table');

button.addEventListener('click', function() {
    button.style.display = 'none';
    table.style.display = 'table';

    console.log("test compleate");
    arrStocks.forEach(generateRow);

    function generateRow(item, i, arr) {
        var domRoot = document.querySelector("tbody");
        var row = document.createElement("tr");
        row.innerHTML = "<td>" + item.CN + "</td>";
        row.innerHTML += "<td>" + item.MC + "</td>";
        row.innerHTML += "<td>" + item.S + "</td>";
        row.innerHTML += "<td>" + item.P + "</td>";
        row.innerHTML += "<td>" + item.NE + "</td>";
        domRoot.appendChild(row);
    }
});