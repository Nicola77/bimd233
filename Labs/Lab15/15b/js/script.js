var pac12 = {};
pac12.logo = ["http://static.pac-12.com.s3.amazonaws.com/images/logos/oregon-logo.png",
    "http://static.pac-12.com.s3.amazonaws.com/images/logos/california-logo.png",
    "http://static.pac-12.com.s3.amazonaws.com/images/logos/washington-logo.png",
    "http://static.pac-12.com.s3.amazonaws.com/images/logos/oregon-state-logo.png",
    "http://static.pac-12.com.s3.amazonaws.com/images/logos/washington-state-logo.png",
    "http://static.pac-12.com.s3.amazonaws.com/images/logos/stanford-logo.png"
];
pac12.school = ["Oregon", "California", "Washington", "Oregon State", "Washington State", "Stanford"];
pac12.conference = ["8-1", "4-5", "4-5", "4-5", "3-6", "3-6"];
pac12.overall = ["12-2", "8-5", "8-5", "5-7", "6-7", "4-8"];
pac12.lastGame = ["W", "W", "W", "L", "L", "L"];
pac12.numSchools = 6;

function genarateRow(row) {
    let str = "";
    str += '<div class="row">';
    str += '<div id="center" class="col"><img src="' + pac12.logo[row] + '" width="32px"></div>';
    str += '<div class="col">' + pac12.school[row] + '</div>';
    str += '<div id="center" class="col">' + pac12.conference[row] + '</div>';
    str += '<div id="center" class="col">' + pac12.overall[row] + '</div>';
    str += '<div id="center" class="col">' + pac12.lastGame[row] + '</div>';
    return str;
}

function genarateTable() {
    let el = document.getElementById("dataPrint");
    el.innerHTML = "";
    for (var i = 0; i < pac12.numSchools; i++) {
        console.log(genarateRow(i));
        el.innerHTML += genarateRow(i);
    }
}

genarateTable();