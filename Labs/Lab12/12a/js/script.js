function calcCircleGeometries(radius) {
    const pi = Math.PI;
    var area = pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter = 2 * radius;
    var geometries = [area, circumference, diameter];
    return geometries;
}

function getRandomNumber() {
    return Math.floor((Math.random() * 10) + 1);
}

function runFunction() {

    for (var i = 1; i <= 3; i++) {
        let res = calcCircleGeometries(getRandomNumber());
        document.getElementById("radius").innerHTML += "<h3>Random radius number " + i + ":</h3>" + "Area: " + res[0] + "<br>Circumference: " + res[1] + "<br>Diameter: " + res[2] + "<br><br>   ";
    }
}

runFunction();