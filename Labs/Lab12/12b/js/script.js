const items = [
    ["2000", "Suzuki", "Grand Vitara", "9,000", "Excelent"],
    ["2000", "Toyota", "Celica", "4,400", "Fair"],
    ["2012", "Toyota", "Highlander", "45,000", "Good"],
    ["2012", "Hyundai", "Elantra", "25,000", "Good"],
]

items.forEach(function(element) {
    $("#rows").append("<tr><th scope=\"row\">" + element[0] + "</th>" +
        "      <td>" + element[1] + "</td>" +
        "      <td>" + element[2] + "</td>" +
        "      <td>" + element[3] + "</td>" +
        "      <td>" + element[4] + "</td></tr>")
})