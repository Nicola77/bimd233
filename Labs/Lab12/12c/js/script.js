setInterval(updateTimer, 1000);

const displayElement = document.getElementById("display")
let i = 0;

function updateTimer() {
    displayElement.innerText = i;
    i++;
}