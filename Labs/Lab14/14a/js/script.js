var wx_data = [
    { day: "Fri", hi: 82, lo: 55 },
    { day: "Sat", hi: 75, lo: 52 },
    { day: "Sun", hi: 69, lo: 52 },
    { day: "Mon", hi: 69, lo: 48 },
    { day: "Tue", hi: 68, lo: 51 }
];

populateTable();

var str = "";
for (var i = 0; i < wx_data.length; i++) {
    //console.log(wx_data[i]);
    str += generateTableRow(wx_data[i]);
    //console.log(str);
}

//
var el = document.getElementById("w-data");
el.innerHTML = str;

/*
   Generates a string that will be used to generate a row in html 
*/
function generateTableRow(wxData) {
    return "<tr>" + "<td>" + wxData.day + "</td>" +
        "<td>" + wxData.hi + "</td>" +
        "<td>" + wxData.lo + "</td>" +
        "<td>" + (wxData.hi + wxData.lo) / 2 + "</td>" +
        "</tr>";
}

/*
    Populates the bootstrap table with high, low, and avg temps for each week 
*/
function populateTable() {
    for (var i = 0; i < wx_data.length; i++) {
        $(".table").append(generateTableRow(wx_data[i]));
    }

    $(".table").append("<tr class='text-warning'> <td>Week Avg: " + wx_data.reduce(getWeekAvg, 0) + "</td> <td></td><td></td><td></td> </tr>");
}

/*
    Returns the week average 
*/
function getWeekAvg(total, currentValue, i, arr) {
    var dayAvg = (currentValue.hi + currentValue.lo) / 2;
    total += dayAvg;
    if (i == arr.length - 1) {
        total = total / arr.length;
    }
    return total;
}