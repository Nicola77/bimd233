let state = "idle";
let cmd = prompt("State: " + state, "next").toLowerCase();;

do {
    document.write("<p>State: " + state + "</p>");
    switch (state) {
        case "idle":
            {
                if (cmd === "run") {
                    state = "S1";
                }
            }
            break;
        case "S1":
            {
                if (cmd === "next") {
                    state = "S2";
                } else if (cmd === "skip") {
                    state = "S3";
                } else if (cmd === "prev") {
                    state = "S4";
                }
            }
            break;
        case "S2":
            {
                if (cmd === "next") {
                    state = "S3";
                }
            }
            break;
        case "S3":
            {
                if (cmd === "next") {
                    state = "S4";
                } else if (cmd === "home") {
                    state = "idle";
                }
            }
            break;
        case "S4":
            {
                if (cmd === "next") {
                    state = "S1";
                }
            }
            break;

        default:
            break;
    }
    cmd = prompt('State: ' + state, 'next').toLowerCase();
} while (cmd != "exit" && cmd != "quit");

document.write("Program has exited with no errors.");