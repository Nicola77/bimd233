const baseURL = "https://api.coindesk.com/v1/bpi/currentprice/";
//let currencyCode = "GBP";
//var myURL = baseURL + currencyCode + ".json";

var chartData = [];

var morris_chart_obj = {
    element: "price-chart",
    data: "",
    xkey: "time",
    ykeys: ["price"]
};

var priceChart = new Morris.Line(morris_chart_obj);


$(document).ready(function() {

    $("button").click(function() {

        $.ajax({
            url: "https://api.coindesk.com/v1/bpi/currentprice.json",
            dataType: "json",
            success: function(data) {
                try {
                    console.log("success");
                } catch (error) {
                    console.log("error");
                }
                var euro = data['bpi']['EUR']['rate_float'].toFixed(2);
                var gbp = data['bpi']['GBP']['rate_float'].toFixed(2);
                var usd = data['bpi']['USD']['rate_float'].toFixed(2);


                var str = "";

                if ($('#currency').val() == "USD") {
                    //currencyCode = 'USD';
                    setInterval(getData("USD"), 1000);
                    str = "Current Value: " + usd + " US Dollars";
                    $('h1').empty().append(str);
                } else if ($('#currency').val() == "EUR") {
                    //currencyCode = 'EUR';
                    setInterval(getData("EUR"), 1000);
                    str = "Current Value: " + euro + " Euros";
                    $('h1').empty().append(str);
                } else if ($('#currency').val() == "GBP") {
                    //currencyCode = 'GBP';
                    setInterval(getData("GBP"), 1000);
                    str = "Current Value: " + gbp + " British Pound Sterling";
                    $('h1').empty().append(str);
                }
            }
        });


    });
});


function getData(currencyCode) {
    let myURL = "https://api.coindesk.com/v1/bpi/currentprice/" + currencyCode + ".json";
    var code = currencyCode;
    console.log("code bef" + currencyCode);
    $.ajax({
        url: myURL,
        success: function(data) {
            console.log(myURL);
            console.log(parseData(data, code));
            renderGraph(data, code);
        }
    });
}

function renderGraph(data, code) {
    plotData(parseData(data, code));
}

function parseData(textPriceData, currencyCode) {
    let bcJSON = JSON.parse(textPriceData);

    // let currencyCode = "EUR";
    console.log(currencyCode);
    let rateText = bcJSON.bpi[currencyCode].rate;
    rateText = rateText.replace(/,/g, "");
    return parseFloat(rateText);
}

function plotData(data) {
    updateData(data);
    priceChart.setData(chartData);
}

function updateData(price) {
    const maxChartValues = 5;
    var newEvent = {
        time: Date.now(),
        price: price
    };

    chartData.push(newEvent);
    if (chartData.length > maxChartValues) {
        chartData.shift();
    }
}