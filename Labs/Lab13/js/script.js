setInterval(updateFlights(10), 5000);

function Flight(airline, flightNumber, origin, destination, depTime, arrivalTime, arrivalGate) {
    this.flightNumber = flightNumber;
    this.airline = airline;
    this.depTime = depTime;
    this.arrivalTime = arrivalTime;
    this.origin = origin;
    this.destination = destination;
    this.arrivalGate = arrivalGate;
    this.flightDuration = new Date(arrivalTime).getTime() - new Date(depTime).getTime();
}

/*
    Converts time from MS to hrs:mins:secs
*/
function displayMS(timeInMS) {
    var seconds = Math.floor((timeInMS / 1000) % 60);
    var minutes = Math.floor((timeInMS / (1000 * 60)) % 60);
    var hours = Math.floor((timeInMS / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
}

/*
    Generates a random departure time and arrival time
*/
function displayDate(dateElement) {
    const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    const month = dateElement.getMonth();
    const day = dateElement.getDate();
    const year = dateElement.getFullYear();

    return months[month] + " " + day + ", " + year;
}

/*
    Returns a formated var with the date and time
*/
function formatDateTime(dateElement) {
    return displayDate(dateElement) + " " + dateElement.toLocaleTimeString();
}

/*
    Generates random data for airport, gate, airline code and dates. Will return a flight object with the random data.
*/
function generateRandomData() {
    var airportCodes = ['SEA', 'FRA', 'HNL', 'OTP', 'LHR', 'AMS', 'MUC', 'DFW', 'PDX', 'MIA', 'MEM', 'NRT', 'ORD', 'YVR'];
    var arivalGates = ['A1', 'A2', 'S15', 'B6', 'A5', 'A6', 'C8', 'S10'];
    var airlineCodes = ['JBU', 'DLH', 'NWA', 'PAA', 'KLM', 'ROT', 'BAW', 'HAL', 'SAS', 'SWR', 'AAL', 'UAE'];

    const datesAndTimes = generateDatesAndTimes();
    return new Flight(airlineCodes[getRandomNumber(11)], getRandomNumber(8000),
        airportCodes[getRandomNumber(11)], airportCodes[getRandomNumber(3)], datesAndTimes[0], datesAndTimes[1], arivalGates[getRandomNumber(3)]);
}

/*
    Generates random a date element
*/
function randomDateAndTime(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

/*
    Generates a random departure time and arrival time
*/
function generateDatesAndTimes() {
    const low = new Date();
    const high = new Date();
    low.setHours(low.getHours() - 6);
    high.setHours(high.getHours() + 6)
    const departureTime = randomDateAndTime(low, new Date());
    const arrivalTime = randomDateAndTime(new Date(), high);
    return [departureTime, arrivalTime];
}

/*
    Updates the in the html bootstrap table for the given number of flights 
*/
function updateFlights(j) {
    for (let i = 1; i <= j; i++) {
        var randomFlight = generateRandomData();
        console.log(randomFlight.flightDuration);
        $(".table").append(
            "<tr><td>" + randomFlight.airline + "</td>" +
            "      <td class='text-warning'>" + randomFlight.flightNumber + "</td>" +
            "      <td>" + randomFlight.origin + "</td>" +
            "      <td>" + randomFlight.destination + "</td>" +
            "      <td>" + formatDateTime(randomFlight.depTime) + "</td>" +
            "      <td>" + formatDateTime(randomFlight.arrivalTime) + "</td>" +
            "      <td>" + randomFlight.arrivalGate + "</td>" +
            "      <td>" + displayMS(randomFlight.flightDuration) + "</td>" +
            "      </tr>");
    }
}

/*
    Generates random number from 0 to i
*/
function getRandomNumber(i) {
    return Math.floor(Math.random() * i);
}